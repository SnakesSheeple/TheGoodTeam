/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */package org.tahomarobotics.robot;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {
	
	// Controller Area Network, CAN, addresses
	public static final int CAN_LEFT_FRONT_MOTOR  = 0;
	public static final int CAN_LEFT_BACK_MOTOR   = 1;
	public static final int CAN_RIGHT_FRONT_MOTOR = 2;
	public static final int CAN_RIGHT_BACK_MOTOR  = 3;
	
	// Digital IO ports
	public static final int LEFT_ENCODER_A  = 0; 
	public static final int LEFT_ENCODER_B  = 1;
	public static final int RIGHT_ENCODER_A = 2;
	public static final int RIGHT_ENCODER_B = 3;
	
	//DigitalInputs

	public static final int LEFT_DIGITAL_INPUT = 4; //TODO: Change these values
	public static final int RIGHT_DIGITAL_INPUT = 5;
	
	//Shift Gear
	public static final int PCM_MODULE = 0;
	public static int shifterForwardChannel = 1;
	public static int shifterReverseChannel = 0;

}
