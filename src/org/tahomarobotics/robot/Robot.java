/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot;

import java.util.ArrayList;
import java.util.List;

import org.tahomarobotics.robot.auto.Autonomous;
import org.tahomarobotics.robot.chassis.Chassis;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;

public class Robot extends IterativeRobot {

	// this holds all of the subsystems and other singleton instances
	private final List<Object> instances = new ArrayList<>();
	
	@Override
	public void robotInit() {
		
		try {
			
			/*
			 * Add instances of all required components
			 * The try-catch will print any initialization errors
			 */
			
			instances.add(OI.getInstance());
			instances.add(Chassis.getInstance());
			
			Chassis.getInstance().calibrate();
			
			
		} catch(Throwable e) {

			e.printStackTrace();
			
			// halt by spinning if failed
			while(true) {
			      // Wait for new data to arrive
			      DriverStation.getInstance().waitForData();
			}
		}
	}
	
	/**
	 * Called every 50 milliseconds
	 */
	@Override
	public void robotPeriodic() {
		// run the commands added to the scheduler
		Scheduler.getInstance().run();
		LiveWindow.run();
	}

	@Override
	public void disabledInit() {
	}

	@Override
	public void disabledPeriodic() {
	}

	@Override
	public void autonomousInit() {
		Autonomous.getInstance().start();
	}

	@Override
	public void autonomousPeriodic() {
	}

	@Override
	public void teleopInit() {
		Autonomous.getInstance().stop();
	}

	@Override
	public void teleopPeriodic() {
	}
}
