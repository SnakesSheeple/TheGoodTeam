/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.chassis;

import org.tahomarobotics.robot.RobotMap;

import com.analog.adis16448.frc.ADIS16448_IMU;
import com.ctre.CANTalon;

import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Timer;
/**
 * Chassis subsystem is responsible for the control and monitoring of the drive base.
 * 
 */
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

@SuppressWarnings("unused")
public class Chassis extends Subsystem {

	public enum Gear {
		LOW, 
		HIGH
	}
	
	// Calibration data for drive system
	public Gear gearState = Gear.LOW;
	private final static double WHEEL_RADIUS = 3.084;
	private final static double STAGE_3_RATIO = 60.0/24;
	private final static double WHEEL_CIRCUMFERENCE = 19.377343487;
	private final static double PULSES_PER_REV = 100;
	private final static double ENCODER_RATIO = 36.0/12;
	
	private final static double DISTANCE_PER_PULSE = 0.02512; // TODO: measure or calculate real value
			
	// Motor controllers	
	private final CANTalon leftFrontMotor = new CANTalon(RobotMap.CAN_LEFT_FRONT_MOTOR);
	private final CANTalon leftBackMotor = new CANTalon(RobotMap.CAN_LEFT_BACK_MOTOR);
	private final CANTalon rightFrontMotor = new CANTalon(RobotMap.CAN_RIGHT_FRONT_MOTOR);
	private final CANTalon rightBackMotor = new CANTalon(RobotMap.CAN_RIGHT_BACK_MOTOR);
	
	// Encoders and Gyroscope
	private final Encoder leftEncoder = new Encoder(RobotMap.LEFT_ENCODER_A, RobotMap.LEFT_ENCODER_B);
	private final Encoder rightEncoder = new Encoder(RobotMap.RIGHT_ENCODER_A, RobotMap.RIGHT_ENCODER_B);
	private final ADIS16448_IMU gyro = new ADIS16448_IMU();
	
	//Digital Input
	private final DigitalInput leftDigitalInput = new DigitalInput(RobotMap.LEFT_DIGITAL_INPUT);
	private final DigitalInput rightDigitalInput = new DigitalInput(RobotMap.RIGHT_DIGITAL_INPUT);
	
	
	// singleton instance
	private static Chassis instance = new Chassis();
	
	//Double Solenoids
	private final DoubleSolenoid shifter = new DoubleSolenoid(RobotMap.PCM_MODULE, RobotMap.shifterForwardChannel , RobotMap.shifterReverseChannel);
	private final Compressor compressor = new Compressor(RobotMap.PCM_MODULE);
	private final AnalogInput pressureSensor = new AnalogInput(RobotMap.PCM_MODULE);

	// private to prevent a second creation 
	private Chassis() {
		
		
		
		leftFrontMotor.changeControlMode(CANTalon.TalonControlMode.PercentVbus);
		leftFrontMotor.setInverted(true);
		leftFrontMotor.setCurrentLimit(30);
		
		leftBackMotor.changeControlMode(CANTalon.TalonControlMode.Follower);
		leftBackMotor.set(leftFrontMotor.getDeviceID());
		leftBackMotor.setCurrentLimit(30);
		
		rightFrontMotor.changeControlMode(CANTalon.TalonControlMode.PercentVbus);
		rightFrontMotor.setCurrentLimit(30);
				
		rightBackMotor.changeControlMode(CANTalon.TalonControlMode.Follower);
		rightBackMotor.set(rightFrontMotor.getDeviceID());
		rightBackMotor.setCurrentLimit(30);
		
		//TODO: CHANGE MOTOR FOLLOW MODES AND SET MAX VOLTAGE
		
		

		leftEncoder.setDistancePerPulse(DISTANCE_PER_PULSE);
		rightEncoder.setDistancePerPulse(DISTANCE_PER_PULSE);
		leftEncoder.reset();
		rightEncoder.reset();
		
		rightEncoder.setReverseDirection(true);
		
		
		
	}
	
	/**
	 * Return the Chassis singleton instance
	 */
	public static Chassis getInstance() {
		return instance;
	}

	/**
	 * Installs the default command for this chassis
	 */
	@Override
	protected void initDefaultCommand() {
		this.setDefaultCommand(new TeleopDrive());
	}
	/**
	 * Sets the power level of the left and right drive systems
	 */
	public void setPower(double left, double right) {
		leftFrontMotor.set(left);
		rightFrontMotor.set(right);
	}
	
	/**
	 * Resets Gyro and encoder to zero position
	 */
	public void reset() {
		leftEncoder.reset();
		rightEncoder.reset();
	}
	
	public void resetGyro() {
		gyro.reset();
	}
	
	/**
	 * Return left drive position in inches
	 */
	public double getLeftPosition() {
		return leftEncoder.getDistance();
	}
	
	/**
	 * Return right drive position in inches
	 */
	public double getRightPosition() {
		return rightEncoder.getDistance();
	}
	
	/**
	 * Return average distance in cthe forward direction
	 */
	public double getForwardPosition() {
		return (leftEncoder.getDistance() + rightEncoder.getDistance())/2.0;
	}
	
	public double getForwardSpeed() {
		return ((rightEncoder.getRate() + leftEncoder.getRate()) / 2.0);
	}
	
	/**
	 * Return the angle of the robot in degrees
	 */
	public double getAngle() {
		return gyro.getAngleZ();
	}

	
	@Override
	public String toString() {
		return String.format("Left %6.3f Right %6.3f Angle %6.3f", 
				getLeftPosition(), getRightPosition(), getAngle());
	}

	/**
	 * calibrates the Gyro
	 */
	public void calibrate() {
		gyro.calibrate();
	}
	public double getGyroZero() {
		return (gyro.getAngleX() + gyro.getAngleY() + gyro.getAngleZ());
	}
	
	public boolean leftBumperSwitchDepressed() {
		return !leftDigitalInput.get();
	}
	
	public boolean rightBumperSwitchDepressed() {
		return !rightDigitalInput.get();
	}
	
	public void GearChange(Gear gear) {
		switch(gear) {
		case HIGH:
			shift(DoubleSolenoid.Value.kReverse);
			break;
		case LOW:
			shift(DoubleSolenoid.Value.kForward);
			break;
		}
	}
	
	public void shift(Value value) {
		shifter.set(value);
	}
	
	public void startCompressor() {
		compressor.start();
	}
	public void stopCompressor() {
		compressor.stop();
	}

	public double getForwardDistance() {
		// TODO Auto-generated method stub
		return (leftEncoder.getDistance() + rightEncoder.getDistance()) / 2.0;
	}
}
