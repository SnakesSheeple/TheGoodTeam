package org.tahomarobotics.robot.chassis;

import org.tahomarobotics.robot.chassis.Chassis;

import edu.wpi.first.wpilibj.command.Command;

public class Forward extends Command {
	private double power;
	private double direction;
	private double distance;
	private double startPos;
	private double endPos;
	Chassis chassis = Chassis.getInstance();
	
	public Forward(double distance, double power) {
		this.distance = distance;
		this.power = power;
		this.direction = Math.signum(Math.abs(distance));
		System.out.println("Forwards Constructed");
	}
	
	protected void initialize(){
		startPos = chassis.getForwardDistance();
		endPos = startPos + distance;
		chassis.setPower(power * direction, power * direction);
		System.out.println("Forwards Intialized");
	}

	protected void execute(){
		
	}
	
	@Override
	protected boolean isFinished() {
		if(direction == 1) {
			System.out.println("Forwards Finished");
			return endPos >= chassis.getForwardDistance();
		}
		else {
			return endPos <= chassis.getForwardDistance();
		}
	}
	
	protected void end() {
		chassis.setPower(0, 0);
	}

}
