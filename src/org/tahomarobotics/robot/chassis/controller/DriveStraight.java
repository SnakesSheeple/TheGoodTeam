package org.tahomarobotics.robot.chassis.controller;

import org.tahomarobotics.robot.chassis.Chassis;
import org.tahomarobotics.robot.chassis.Chassis.Gear;

import edu.wpi.first.wpilibj.command.Command;


public class DriveStraight extends Command {

	
	
	double distance;
	double speed;
	double startPosition;
	double endPosition;
	double direction;
	Chassis chassis = Chassis.getInstance();
	ChassisController controller = new ChassisController(chassis);

	public DriveStraight(double distance, double speed) {
		requires (chassis);
		chassis.reset();
		this.distance = distance;
		this.speed = speed;
		direction = Math.signum(distance);
		
	}
	protected void initialize() {
		startPosition = chassis.getForwardDistance();
		endPosition = startPosition + distance;
		chassis.GearChange(Gear.LOW);
		if(speed > 1.0) {
			speed = 1.0;
		}
		controller.startControl(distance, 0, speed, 0);
	}
	protected void execute(){
			
		
	}

	@Override
	protected boolean isFinished() {
		return controller.onTarget();
	}
	
	protected void end() {
		controller.stopConrtol();
		chassis.setPower(0, 0);
	}
}
