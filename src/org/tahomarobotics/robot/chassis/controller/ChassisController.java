/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.chassis.controller;

import org.tahomarobotics.robot.chassis.Chassis;

import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;

public class ChassisController {

	private volatile double forwardPower = 0;
	private volatile double rotatePower = 0;
	private final PIDController rotatePID;
	private final PIDController forwardPID;
	private final Chassis chassis;
	private AbsoluteTolerance rotateTolerance;
	private AbsoluteTolerance forwardTolerance;
	
	public ChassisController(Chassis chassis) {
		this.chassis = chassis;
		
		PIDSource forwardSource = new PIDSource() {
			@Override
			public void setPIDSourceType(PIDSourceType pidSource) {
			}
			@Override
			public PIDSourceType getPIDSourceType() {
				return PIDSourceType.kDisplacement;
			}
			@Override
			public double pidGet() {
				return chassis.getForwardPosition();
			}
		};
		
		PIDSource rotateSource = new PIDSource() {
			@Override
			public void setPIDSourceType(PIDSourceType pidSource) {
			}
			@Override
			public PIDSourceType getPIDSourceType() {
				return PIDSourceType.kDisplacement;
			}
			@Override
			public double pidGet() {
				return chassis.getAngle();
			}
		};
		
		PIDOutput forwardOutput = new PIDOutput() {
			@Override
			public void pidWrite(double output) {
				forwardPower = output;
				chassis.setPower(forwardPower + rotatePower, forwardPower - rotatePower);
			}
		};
		
		PIDOutput rotateOutput = new PIDOutput() {
			@Override
			public void pidWrite(double output) {
				rotatePower = output;
				chassis.setPower(forwardPower + rotatePower, forwardPower - rotatePower);
			}
		};

		rotatePID = new PIDController(0.007, 0.0003, 0.07, rotateSource, rotateOutput);
		forwardPID = new PIDController(0.1, 0.0, 0.0, forwardSource, forwardOutput);

		setRotationTolerance(10, 2.0);
		setForwardTolerance(10, 1.0);
	}
	
	public void setRotationTolerance(int sampleCount, double absTolerance) {
		rotateTolerance = new AbsoluteTolerance(rotatePID, sampleCount, absTolerance);
		rotatePID.setTolerance(rotateTolerance);
	}
	
	public void setForwardTolerance(int sampleCount, double absTolerance) {
		forwardTolerance = new AbsoluteTolerance(forwardPID, sampleCount, absTolerance);
		forwardPID.setTolerance(forwardTolerance);
	}
	
	/**
	 * Starts control of forward and rotational PID controller with provided distance and angle.
	 * 
	 * @param forwardDistance - distance in inches forward or backward from current position
	 * @param rotateAngle - angle in degrees clockwise or counter-clockwise
	 */
	public void startControl(double forwardDistance, double rotateAngle) {
		startControl(forwardDistance, rotateAngle, 1, 1);
	}
	
	/**
	 * Starts control of forward and rotational PID controller with provided distance and angle.
	 * 
	 * @param forwardDistance - distance in inches forward or backward from current position
	 * @param rotateAngle - angle in degrees clockwise or counter-clockwise
	 */
	public void startControl(double forwardDistance, double rotateAngle, double maxForwardSpeed, double maxRotateSpeed) {
		stopConrtol();
		
		// TODO: add code to limit speed
		
		double startForwardPosition = chassis.getForwardPosition();
		double startRotatePosition = chassis.getAngle();
		forwardPID.setSetpoint(forwardDistance + startForwardPosition);
		forwardPID.enable();
		rotatePID.setSetpoint(rotateAngle + startRotatePosition);
		rotatePID.enable();
	}

	/**
	 * Stops active control of the chassis drive
	 */
	public void stopConrtol() {
		forwardPID.disable();
		rotatePID.disable();
		forwardTolerance.reset();
		rotateTolerance.reset();
	}
	
	/**
	 * Return true when both forward and rotation distance are satisfied
	 */
	public boolean onTarget() {
		boolean onTarget = true;
		onTarget &= forwardPID.onTarget();
		onTarget &= rotatePID.onTarget();
		return onTarget;
	}
	
	public String onTargetString() {
		if (forwardPID.onTarget() && rotatePID.onTarget()) {
			return "Both";
		} else if (forwardPID.onTarget()) {
			return "Fwd ";
		} else if (rotatePID.onTarget()) {
			return "Rot ";
		} else {
			return "None";
		}
	}
	
	public String toString() {
		return String.format("Fwd %6.3f Ang: %6.3f Ena: %s onTgt: %s", 
				chassis.getForwardPosition(),
				chassis.getAngle(),
				(forwardPID.isEnabled() || rotatePID.isEnabled()) ? "On " : "Off",
				onTargetString());
	}

	public double getRotateError() {
		return rotateTolerance.getAverageError();
	}

}

