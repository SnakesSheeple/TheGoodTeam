/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.chassis.controller;

import edu.wpi.first.wpilibj.PIDController.Tolerance;
import edu.wpi.first.wpilibj.PIDInterface;

/**
 * This replaces the AbsoluteTolerance from WPI which has a bug with average 
 * the error rather than the Math.abs(error).  Without using the absolute operator,
 * a negative error and positive error may average to near zero.
 *
 */
public class AbsoluteTolerance implements Tolerance {

	private final PIDInterface controller;
	private final double[] samples;
	private final double absTolerance;

	private int currentSamples;
	private int samplePosition;
	private double averageError;
	
	public AbsoluteTolerance(PIDInterface controller, int sampleCount, double absTolerance) {
		this.controller = controller;
		this.samples = new double[sampleCount];
		this.absTolerance = Math.abs(absTolerance);
	}
	
	@Override
	public boolean onTarget() {
		
		sampleError();
		
		if (currentSamples < samples.length) {
			return false;
		}
		
		double sum = 0;
		for(double sample : samples) {
			sum += sample;
		}
		averageError = sum / samples.length;
		
		boolean onTarget = averageError <= absTolerance;
	
		return onTarget;
	}
	
	public void reset() {
		currentSamples = 0;
		samplePosition = 0;		
	}
	
	private void sampleError() {
		samples[samplePosition] = Math.abs(controller.getError());
		
		currentSamples++;
		samplePosition++;
		samplePosition %= samples.length;
	}
	
	public double getAverageError() {
		return averageError;
	}

}
