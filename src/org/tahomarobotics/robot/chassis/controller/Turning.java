package org.tahomarobotics.robot.chassis.controller;

import org.tahomarobotics.robot.chassis.Chassis;
import org.tahomarobotics.robot.chassis.Chassis.Gear;

import edu.wpi.first.wpilibj.command.Command;

public class Turning extends Command {

	
	
	double distance;
	double angle;
	double startPosition;
	double endPosition;
	double direction;
	Chassis chassis = Chassis.getInstance();
	ChassisController controller = new ChassisController(chassis);

	public Turning(double angle) {
		System.out.println("Turning Constructed &&&");
		requires (chassis);
		chassis.reset();
		this.angle = angle;
		
	}
	protected void initialize() {
		direction = Math.signum(angle);
		startPosition = chassis.getAngle();
		endPosition = startPosition + angle;
		System.out.println("Turning Initialized &&&");
		controller.startControl(0, angle, 0, 0.5);
		chassis.GearChange(Gear.LOW);
	}
	protected void execute(){
			
		
	}

	@Override
	protected boolean isFinished() {
			System.out.printf("%8.3f %s\n",chassis.getAngle(), controller.onTargetString());
			return controller.onTarget();
	}
	
	protected void end() {
		controller.stopConrtol();
	}
}
