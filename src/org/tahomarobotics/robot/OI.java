/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot;

import org.tahomarobotics.robot.auto.BackUpCommand;
import org.tahomarobotics.robot.auto.BackUpTillTote;
import org.tahomarobotics.robot.auto.NoOp;
import org.tahomarobotics.robot.auto.shiftDown;
import org.tahomarobotics.robot.auto.shiftUp;
import org.tahomarobotics.robot.chassis.controller.DriveStraight;
import org.tahomarobotics.robot.chassis.controller.Turning;

import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.buttons.JoystickButton;

/**
 * Operator Interface, OI, is used to connect the robot to the controls of the Driver 
 * Station where the Drive Team operates the controls.  It should connect robot command
 * to various controller buttons, sticks and triggers.  Additional controllers can be 
 * added if needed. 
 *
 */
public class OI {

	// single instance of OI
	private static final OI instance = new OI();

	// hand controller
	private final XboxController driverController = new XboxController(0);
	

	private static final double DEAD_BAND = 12 / 127.0;

	// event buttons
	private final JoystickButton driverButtonA = new JoystickButton(driverController, 1);
	private final JoystickButton driverButtonB = new JoystickButton(driverController, 2);
	private final JoystickButton driverButtonX = new JoystickButton(driverController, 3);
	private final JoystickButton driverButtonY = new JoystickButton(driverController, 4);
	private final JoystickButton driverLeftBumper = new JoystickButton(driverController, 5);
	private final JoystickButton driverRightBumper = new JoystickButton(driverController, 6);
	
	
	/**
	 *  default constructor, this is private ensuring there is only 
	 *  one instance as defined above
	 */
	private OI() {
		// TODO: change code here
		driverButtonA.whenPressed(new Turning(90));
		driverButtonB.whenPressed(new DriveStraight(24, 0.5));
		driverButtonX.whenPressed(new BackUpTillTote());
		driverButtonY.whenPressed(new NoOp());
		driverLeftBumper.whenPressed(new shiftDown());
		driverRightBumper.whenPressed(new shiftUp());
		
		
	}
	
	/**
	 * Returns the one instance of OI
	 */
	public static OI getInstance() {
		return instance;
	}
	
	private double getDriverLeftYAxis() {
		return -applyDeadBand(driverController.getY(Hand.kLeft));
	}

	private double getDriverRightYAxis() {
		return -applyDeadBand(driverController.getY(Hand.kRight));
	}
	
	/**
	 * Returns the forward drive power.  It is an average of the left and
	 * right stick.
	 */
	public double getForwardPower() {
		return desensitize((getDriverLeftYAxis() + getDriverRightYAxis()) / 2.0);
	}

	/**
	 * Returns the rotational drive power. It is difference of the left and right
	 * stick.
	 */
	public double getRotatePower() {
		return desensitize((getDriverLeftYAxis() - getDriverRightYAxis()) / 2.0);
	}
	
	/**
	 * Return the commanded left drive power
	 */
	public double getLeftDrivePower() {
		return getForwardPower() + getRotatePower();
	}
	
	/**
	 * Return the commanded right drive power
	 */
	public double getRightDrivePower() {
		return getForwardPower() - getRotatePower();
	}
	
	/**
	 * Removes small values emitted from the controller axis when stick is relaxed.
	 * 
	 * Returns the value only if value isn't in the range of -DEAD_BAND to +DEAD_BAND
	 */
	private double applyDeadBand(double value){
		return Math.abs(value) > DEAD_BAND ? value : 0.0;
	}
	
	/**
	 * Desensitizes the passed in value to make the robot more controllable
	 */
	private double desensitize(double value) {
		return Math.pow(Math.abs(value), 1) * value;
		
	}
}
