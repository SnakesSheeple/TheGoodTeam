/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.auto;

import org.tahomarobotics.robot.chassis.Forward;
import org.tahomarobotics.robot.chassis.Rotate;
import org.tahomarobotics.robot.chassis.controller.DriveStraight;
import org.tahomarobotics.robot.chassis.controller.Turning;
import org.tahomarobotics.robot.auto.BackUpTillTote;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.command.WaitCommand;

public class BackInFarSideOfObstruction extends CommandGroup {

	public BackInFarSideOfObstruction() {
		addSequential(new Turning(45.0));
		addSequential(new DriveStraight(60.0, 1.0));
		addSequential(new Turning(-45.0));
		addSequential(new DriveStraight(36.0, 1.0));
		addSequential(new Turning(-45.0));
		addSequential(new DriveStraight(84.0, 1.0));
		addSequential(new Turning(45.0));
		addSequential(new BackUpTillTote());
		Timer.delay(2);
		addSequential(new DriveStraight(6.0, 1.0));
		addSequential(new Turning(-45.0));
		addSequential(new DriveStraight(114.0, 1.0));
		addParallel(new NoOp());
	}
}
