package org.tahomarobotics.robot.auto;

import org.tahomarobotics.robot.chassis.Chassis;

import edu.wpi.first.wpilibj.command.Command;

public class BackUpTillTote extends Command {
	Chassis chassis = Chassis.getInstance();
	
	
	//called when the code first realizes that this command exsists
	public BackUpTillTote() {
		requires(chassis);
	}
	
	
	//called when this command is started
	protected void initialize() {

		this.setTimeout(5);
		chassis.setPower(-0.2, -0.2);
		
	}
	
	
	//called repeatedly while command is running;
	protected void execute() {
		
	}
	
	
	//called repeatedly while command is running, if it returns true the command ends
	@Override
	protected boolean isFinished() {
		return (chassis.leftBumperSwitchDepressed() || chassis.rightBumperSwitchDepressed()) || this.isTimedOut();
	}
	
	//called when isFinished returns true and the command is ending
	protected void end() {
		chassis.setPower(0, 0);
	}

}
