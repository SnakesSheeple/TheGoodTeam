/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.auto;

import org.tahomarobotics.robot.chassis.controller.DriveStraight;
import org.tahomarobotics.robot.chassis.controller.Turning;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * Autonomous is used to manage the selection of the commands and command groups for
 * the autonomous phase of the robot.  The Drive Team will need to select the command
 * from one or more Smart Dash-board choosers (pick list).
 * 
 * Once selected on the driver station, autonomous initialization will call start running
 * the selected command(s).
 * 
 * It is importance to stop this command at the end of autonomous just in chase it is still
 * running.  Stop is called teleop initialization to do this.
 */
public class Autonomous {

	// single instance of Autonomous
	private static final Autonomous instance = new Autonomous();
	@SuppressWarnings("rawtypes")
	SendableChooser autoChooser = new SendableChooser();
	
	private Command selectedAutoCommand = null;
	
	/**
	 *  default constructor, this is private ensuring there is only 
	 *  one instance as defined above
	 */
	private Autonomous() {
		setupSelectionOptions();
	}
	
	@SuppressWarnings("unchecked")
	private void setupSelectionOptions() {
		autoChooser.addDefault("No Operation", new NoOp());
		autoChooser.addObject("Drive Course", new BackInFarSideOfObstruction());
		autoChooser.addObject("Drive Box", new DriveBox());
		SmartDashboard.putData("Autonomus Mode Chooser", autoChooser);
		SmartDashboard.putData("Forwards 2 feet", new DriveStraight(24.0, 0.5));
		SmartDashboard.putData("Turn 90 degrees", new Turning(90));
				
	}
	
	private Command getSelectCommand() {
		
		Command autonomusChoice = (Command) autoChooser.getSelected();
		selectedAutoCommand = autonomusChoice;
		return autonomusChoice;
	}
	
	/**
	 * Returns the one instance of Autonomous
	 */
	public static final Autonomous getInstance() {
		return instance;
	}
	
	/**
	 * This takes the selected autonomous command and start it by adding it
	 * to the scheduler.  This should be called by Robot in autonomousInit().
	 */
	public final void start() {
		selectedAutoCommand = getSelectCommand();
		if (selectedAutoCommand != null) {
			selectedAutoCommand.start();
		}
	}
	
	/**
	 * This takes the selected autonomous command and stops it by canceling it.
	 * This should be called by Robot in teleopInit().
	 */
	public final void stop() {
		if (selectedAutoCommand != null) {
			if (selectedAutoCommand.isRunning()) {
				selectedAutoCommand.cancel();
			}
		}
	}
}
