package org.tahomarobotics.robot.auto;

import org.tahomarobotics.robot.chassis.Chassis;
import org.tahomarobotics.robot.chassis.Chassis.Gear;

import edu.wpi.first.wpilibj.command.Command;

public class shiftUp extends Command {
	
	public void initialize() {
		Chassis chassis = Chassis.getInstance();
		chassis.GearChange(Gear.HIGH);
	}

	@Override
	protected boolean isFinished() {
		// TODO Auto-generated method stub
		return true;
	}

}
