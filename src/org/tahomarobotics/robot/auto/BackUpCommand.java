package org.tahomarobotics.robot.auto;

import edu.wpi.first.wpilibj.command.CommandGroup;

public class BackUpCommand extends CommandGroup {
	public BackUpCommand() {
		addSequential(new BackUpTillTote());
	}

}
