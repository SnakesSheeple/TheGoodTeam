package org.tahomarobotics.robot.auto;

import org.tahomarobotics.robot.chassis.Chassis;

import edu.wpi.first.wpilibj.command.Command;

public class Encoder100Ticks extends Command{
	public double distance = 100;
	public double power = 0.5;
	public double startPos;
	Chassis chassis = Chassis.getInstance();
	 //\\
	//  \\
	protected void initialize() {
		startPos = chassis.getForwardPosition();
		chassis.setPower(power, power);
	}
	protected void execute() {
		
	}

	@Override
	protected boolean isFinished() {
		return (chassis.getForwardPosition() >= distance);
		
		
	}
	
	protected void end() {
		chassis.setPower(0, 0);
	}

}
