package org.tahomarobotics.robot.auto;

import org.tahomarobotics.robot.chassis.Rotate;
import org.tahomarobotics.robot.chassis.controller.DriveStraight;

import edu.wpi.first.wpilibj.command.CommandGroup;

public class DriveBox extends CommandGroup {
	public DriveBox() {
		
		addSequential(new DriveStraight(24.0, 0.5));
		addSequential(new Rotate(90.0));
		addSequential(new DriveStraight(24.0, 0.5));
		addSequential(new Rotate(90.0));
		addSequential(new DriveStraight(24.0, 0.5));
		addSequential(new Rotate(90.0));
		addSequential(new DriveStraight(24.0, 0.5));
	}
}
