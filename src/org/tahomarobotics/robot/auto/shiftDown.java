package org.tahomarobotics.robot.auto;

import org.tahomarobotics.robot.chassis.Chassis;
import org.tahomarobotics.robot.chassis.Chassis.Gear;

import edu.wpi.first.wpilibj.command.Command;

public class shiftDown extends Command {
	
	public void initialize() {
		Chassis chassis = Chassis.getInstance();
		chassis.GearChange(Gear.LOW);
	}

	@Override
	protected boolean isFinished() {
		// TODO Auto-generated method stub
		return true;
	}

}
